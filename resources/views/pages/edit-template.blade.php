@extends('adminlte::page')

@section('plugins.Datatables', true)

@section('content')
{{-- {{dd($data)}} --}}
<div class="container">
<form id="form_temp" name="form_temp" class="form">
    <div class="row justify-content-center">
        <div class="col-md-12" id="div_2">
            <div class="card">
            <h5 class="card-header">Asistente para editar plantilla: <label id="tipoLabel"></label></h5>
            <div class="card-body">
                <div class="form-row">
                    <div class="form-group col-md-10"></div>
                    <div class="form-group col-md-2">
                        <label><label style="color: blue;">*</label> Campos obligatorios</label>
                    </div>
                </div>
            </div>
                <div class="card-body">
                    <div class="form-row">

                        <div class="form-group col-md-3" id="div_tipo_art">
                            <label data-bs-toggle="tooltip" data-bs-placement="top" title="Campo Obligatorio.&#10;Area de la cual es el articulo">Tipo de artículo</label><label style="color: blue;">*</label>
                            @foreach ($tipos_art as $item)
                                <div class="form-check">
                                    <input class="form-check-input" value='{{$item->id}}' type="radio" name="tipo_art" id="tipo_{{$item->id}}">
                                    <label class="form-check-label">{{$item->tipo}}</label>
                                </div>
                            @endforeach
                        </div>

                        <div class="form-group col-md-1"></div>

                        <div class="form-group col-md-4" id="div_aplicacion">
                            <label data-bs-toggle="tooltip" data-bs-placement="top" title="Campo Obligatorio.&#10;Para que se usa el articulo">Aplicación</label><label style="color: blue;">*</label>
                            <select class="form-control" id="aplicacion" name="aplicacion"></select>
                        </div>

                        <div class="form-group col-md-4" id="div_marca">
                            <label data-bs-toggle="tooltip" data-bs-placement="top" title="Campo Obligatorio.&#10;Marca del articulo">Marca</label><label style="color: blue;">*</label>
                            <select class="form-control" id="marcas" name="marcas"></select>
                        </div>
                    </div>

                    <div class="form-row">

                        <div class="form-group col-md-3" id="div_num_part">
                            <label data-bs-toggle="tooltip" data-bs-placement="top" title="Campo opcional.&#10;Numero de parte del articulo">Número de parte</label>
                            <input class="form-control" type="text" id="num_parte" name="num_parte">
                        </div>

                        <div class="form-group col-md-1"></div>
                        
                        <div class="form-group col-md-3" id="div_art_oracle">
                            <label data-bs-toggle="tooltip" data-bs-placement="top" title="Campo Obligatorio.&#10;Código del articulo en Oracle">Artículo Oracle</label><label style="color: blue;">*</label>
                            <input class="form-control" type="text" id="art_oc" name="art_oc">
                        </div>

                        <div class="form-group col-md-8">
                            <label data-bs-toggle="tooltip" data-bs-placement="top" title="Campo Obligatorio.&#10;Descripcion del articulo">Descripción del articulo</label><label style="color: blue;">*</label>
                            <textarea class="form-control" type="text" id="desc" name="desc"></textarea>
                        </div>
                    </div>

                    <div class="form-row">

                        <div class="form-group col-md-3" id="div_u_medida">
                            <label data-bs-toggle="tooltip" data-bs-placement="top" title="Campo Obligatorio.&#10;Unidad de medida del articulo">Unidad de medida</label><label style="color: blue;">*</label>
                            <select class="form-control" id="medida" name="medida">
                                <option value="" selected>Selecciona la unidad de medida</option>
                                @foreach($unidades_medida as $um)
                                    <option value='{{$um->id}}'>{{$um->unidad_medida}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-1"></div>

                        <div class="form-group col-md-2">
                            <label data-bs-toggle="tooltip" data-bs-placement="top" title="Campo Obligatorio.&#10;Precio del articulo">Precio</label><label style="color: blue;">*</label>
                            <div class="input-group mb-3">
                                <span class="input-group-text">$</span><input class="form-control" type="decimal" id="precio" name="precio" min="1" >
                            </div>
                        </div>

                        <div class="form-group col-md-7"></div>
                        
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12" id="div_plantas">
                            <label>Plantas</label><label style="color: blue;">*</label>
                        </div>
                        <div class="form-group col-md-6 plt">
                            <label class="text-secondary">Plantas PRESER</label>
                            <select class="select" id="planta_preser" name="planta_preser" multiple>
                                @foreach ($plantas_p as $planta)
                                    <option value="{{$planta->id}}">{{$planta->Codigo}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6 plt">
                            <label class="text-secondary">Plantas SEEMEX</label>
                            <select class="select" id="planta_seemex" name="planta_seemex" multiple>
                                @foreach ($plantas_s as $planta)
                                    <option value="{{$planta->id}}">{{$planta->Codigo}}</option>
                                @endforeach
                            </select>
                        </div>
                        @foreach ($plantas as $planta)
                            <div class="form-group col-md-3 plt">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="{{$planta->id}}" name="planta_{{$planta->id}}" id="{{$planta->id}}">
                                    <label class="form-check-label" id="planta_{{$planta->id}}">{{$planta->Codigo}}</label>
                                </div>
                            </div>
                        @endforeach
                        <div class="form-group col-md-3 plt">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="{{$razon}}" name="planta_{{$razon}}" id="{{$razon}}">
                                <label class="form-check-label" id="planta_{{$razon}}">Todas</label>
                            </div>
                        </div>
                        <div class="form-group col-md-3" id="todas_plt">
                            <div class="form-check">
                                <label class="text-secondary" id="planta_666">Todas las plantas seleccionadas</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-10"></div>
                        <div class="form-group col-md-2">
                            {{-- <button type="submit" class="btn btn-primary">Generar</button> --}}
                            <button class="btn btn-primary" type="button" onclick="addElement()">Agregar</button>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</form>
<form id="form_2" name="form_2" class="form">
    <div class="card" id="div_adjunto">
        <div class="card-body">
            <div class="form-row">
                <div class="form-group col-md-3">
        			<label class="form-label">Seleccionar archivo </label>
    		    </div>
		    
		        <div class="form-group col-md-1"></div>
		    
    		    <div class="form-group col-md-1">
            	    <input type="file" name="archivo" title="seleccionar fichero" id="importData" accept=".xls,.xlsx" />
            	</div>
            </div>
        </div>
        <div class="form-group col-md-8">
            <label>Comentarios</label>
            <textarea class="form-control" type="text" id="coment" name="coment"></textarea>
        </div>
    </div>
</form>
<div class="card" id="div_3">
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                <table class="table table-sm table-striped table-bordered" id="articlesDT" class="display">
                </table>
                <button id="generar" class="btn btn-primary" onclick="save()">Generar</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal_art" tabindex="-1" role="dialog" aria-labelledby="modalTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="form-group col-md-6">
                    <h5 class="modal-title">Articulos semejantes</h5>
                </div>
                <div class="form-group col-md-4"></div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-sm table-striped table-bordered" id="t-art" style="width:100%">
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')  
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">

    let idA = null;
    let tipo_t = "";
    let artDT;
    let matchDT;
    let temporal;

    $('#modal_art').on('hidden.bs.modal', function () {
        $('#matchDT').DataTable().destroy().draw();
        $('#matchDT').remove();
    });

    function outputUpdate(val){
        if(val == 1){
            document.querySelector('#val').value = "Baja";
        }else if(val == 2){
            document.querySelector('#val').value = "Media";
        }else if(val == 3){
            document.querySelector('#val').value = "Alta";
        }else if(val == 4){
            document.querySelector('#val').value = "Crítica";
        }
        
    }

    $(document).ready(function () {
        $("#div_2").hide();
        $("#div_3").hide();
        $("#planta_preser").select2();
        $("#planta_seemex").select2();
        $('#articlesDT').DataTable({
            ajax: {
                "url": '{{ url("articulos-get") }}/{{ $solicitud }}',
                "method": "GET",
                "dataSrc": "" /* (json) => {
                    console.log("iam in", json);
                } */
            },
            columnDefs: [
                {
                    orderable: false,
                    targets: [10, 11, 12]
                },
                /// Hide Columns
                {
                    targets: [0,1],
                    visible: false,
                    searchable: false
                }
            ],
            language: {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                "infoEmpty": "Mostrando 0 a 0 de 0 Entradas",
                "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Entradas",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            },
            columns: [
                {
                    title: "Prioridad",
                    data: 'prioridad'
                },
                {
                    title: "Tipo Ticket",
                    data: 'tipo'
                },
                { 
                    title: "Tipo Articulo",
                    data: 'tipo_art',
                    render: function (data, type, full, meta) {
                        return $(`#tipo_${data}`).next().text();
                    }
                },
                {
                    title: "Aplicacion",
                    data: 'aplicacion',
                    render: function (data, type, full, meta) {
                        return data == ''?'':$("#aplicacion").find(`option[value=${data}]`).text();
                    }
                },
                {
                    title: "Marca",
                    data: 'marca',
                    render: function (data, type, full, meta) {
                        return data == ''?'':$("#marcas").find(`option[value=${data}]`).text();
                    }
                },
                {
                    title: "Num Parte",
                    data: 'num_parte'
                },
                {
                    title: "Desc",
                    data: 'descripcion'
                },
                { 
                    title: "Medida",
                    data: 'unidad_medida',
                    render: function (data, type, full, meta) {
                        console.log(data??'' == '')
                        
                        return data == ''|| data == undefined ?'':document.getElementById("medida").options[data].text;
                        
                    }
                },
                {
                    title: "Precio",
                    data: 'precio',
                    render: function (data, type, full, meta) {
                        return `$${parseFloat(data).toFixed(2)}`;
                    } 
                },
                { 
                    title: "Planta",
                    data: 'planta',
                    //TODO: Si es un array agregar la opcion de mostrar lista al poner el puntero Array.isArray(data)
                    render: function (data, type, full, meta) {
                        return data.length>1 
                        ? `${data.length} plantas`
                        // : (data[0]??1)?.value??'';
                        : $(`#planta_${data[0]}`).text();
                        // : data;
                    } 
                },
                {
                    title: "Articulo Oracle",
                    data: 'art_orc'
                },
                { 
                    title: "Existe",
                    data: "exist",
                    render: (data, type, full, meta) => {
                        return data[1].length>0?`<a onclick="openM(${data[0]})">
                            <i class="fas fa-exclamation-triangle" style="color: Tomato;">
                        </a>`:'';
                    },
                },
                { 
                    title: "",
                    data: "edit",
                    render: function (data, type, full, meta) {
                        return `<div class="mb-3"><button type="button" class="btn btn-info btn-sm" onclick="editData(${data})">Editar</button><br></div>`;
                    } 
                },
                { 
                    title: "",
                    data: "delete",
                    render: function (data, type, full, meta) {
                        return `<div class="mb-3"><button type="button" class="btn btn-danger btn-sm" onclick="deleteRow(${data})">Eliminar</button><br></div>`;
                    }
                }
            ],
            initComplete: (settings, json) => {
                console.log(json);
                artDT = $('#articlesDT').DataTable();
                tipo_t = json[0]["tipo"];
                
                if(tipo_t=="Alta"){
                    $(".plt").hide();
                    $("#div_art_oracle").hide();
                    [0,1,10].forEach(d=>{
                        artDT.column(d).visible(false);
                    })
                } else {
                    [0,1,3,4,5,7,11].forEach(d=>{
                        artDT.column(d).visible(false);
                    })
                    $("#todas_plt").hide();
                    $("#div_num_part").hide();
                    $("#div_marca").hide();
                    $("#div_u_medida").hide();
                    $("#div_aplicacion").hide();
                }
                document.getElementById("coment").value = json[0]["comentario"];
            }
        });
        matchDT = new $('#t-art').DataTable({
            data: [],
            columns: [
                {
                    title: 'Código de artículo',
                    width: '20%'
                },
                {title: 'Descripción'}
            ]
        });
        $("#div_2").show();
        $("#div_3").show();
        
        fetch("{{url('select2-marcas')}}",{
            method: 'GET'
        }).then(r=>r.json())
        .then(d => {
            $('#marcas').select2({
                data:d,
                tags: true
            });
            $('#marcas').val(null).trigger('change');
        }).catch(err => {
            console.log(err);
        });
            
        fetch("{{url('select2-aplicacion')}}",{
            method: 'GET'
        }).then(r=>r.json())
        .then(d => {
            $('#aplicacion').select2({
                tags: true,
                data:d
            });
            $('#aplicacion').val(null).trigger('change');
        }).catch(err => {
            console.log(err);
        });

    });

    function openM(arts) {
        $("#modal_art").modal("show");
        arts = artDT.row(`#${arts}`).data().exist[1];
        
        matchDT.clear().draw();
        
        matchDT.rows.add(arts);

        matchDT.columns.adjust().draw();
    }

    function addElement() {
        //TODO: Aplicacion y marca no dejan agregar campos que no existen
        let element = {};
        element.planta = [];

        $("#form_temp").serializeArray().forEach(e => {
            if (e.name.includes("planta")) {
                element.planta.push(e.value);
            } /* else if (e.name == "aplicacion" || e.name == "marcas") {
                element.name = e;
            }  */else {
                element[e.name] = e.value;
            }
        });

        let errores = '';
        let hasError = false;
        element.tipo_tick=tipo_t;
        if (element.tipo_tick=="Alta") {
            element.planta.push("666")
            
            if (element.desc=='') {
                errores += 'Ingrese una descripción\n';
                hasError = true;
            }
            if (element.aplicacion==undefined) {
                errores += 'Ingrese una aplicacion\n';
                hasError = true;
            }
            if (element.marcas==undefined) {
                errores += 'Ingrese una marca\n';
                hasError = true;
            }
            if (element.medida=='') {
                errores += 'Seleccione una unidad de medida\n';
                hasError = true;
            }
        }else{
            if(element.art_oc==''){
                errores += 'Ingrese un codigo de articulo de Oracle Cloud\n';
                hasError = true;
            }
        }
        console.log(element);
        if (element.tipo_art==undefined) {
            errores += 'Seleccione un tipo de articulo\n';
            hasError = true;
        }
        if (!(/(^[1-9][0-9]*(.[0-9]{0,2})?$)/.test(element.precio))) {
            errores += 'Ingrese un precio valido\n';
            hasError = true;
        }
        if (element.planta.length==0) {
            errores += 'Seleccione al menos una planta\n';
            hasError = true;
        }

        if (hasError) {
            swal("Oops!", errores, "error");
            return;
        }

        let art = {};
        let id = idA==null?artDT.rows().count():idA;

        art.prioridad = 0;
        art.tipo = 0;
        art.tipo_art = element.tipo_art;
        art.aplicacion = element.aplicacion??0;
        art.marca = element.marcas??0;
        art.num_parte = element.num_parte;
        art.descripcion = element.desc;
        art.unidad_medida = element.medida??'';
        art.precio = element.precio;
        art.planta = element.planta;
        art.planta = element.planta;
        art.art_orc = element.art_oc;
        art.exist = [id, []];
        art.edit = id;
        art.delete = id;
        art.DT_RowId = id;
        artDT.row.add(
            art
        );
        $('input:checkbox').prop('checked', false);
        $('#num_parte').val('');
        $('#desc').val('');
        $('#medida').val('Selecciona la unidad de medida');
        $('#precio').val('');
        $('#aplicacion').val(null).trigger('change');
        $('#marcas').val(null).trigger('change');
        $('#art_oc').val('');
        artDT.draw();

        if (element.tipo_tick=="Alta") {
            
            $.ajax({
                url: '{{url("articulos/get")}}',
                dataType: 'json',
                type: 'POST',
                data: {
                    num_parte: element.num_parte,
                    desc: element.desc
                },
                success: (res) => {
                    
                    if (res.length==0) {
                        console.log("empty response");
                        return;
                    }
                    artDT.cell(artDT.row(`#${id}`).index(), 11).data([id, res]);
                },
                error: (conf, text, error) => {
                    console.log("Error:");
                }
            });
        }
        
        idA = id+1;
    }

    function deleteRow(row) {
        artDT.rows(`#${row}`).remove().draw();
    }

    function editData(row) {
        let id = row;
        row = artDT.row(`#${row}`).data();
        console.log(row);
        document.form_temp.tipo_art.value=row["tipo_art"];
        $('#aplicacion').val(row["aplicacion"]).trigger("change");
        $('#marcas').val(row["marca"]).trigger("change");
        $('#num_parte').val(row["num_parte"]);
        $('#desc').val(row["descripcion"]);
        $('#medida').val(row["unidad_medida"]);
        $('#precio').val(row["precio"]);
        $('#art_oc').val(row["art_orc"]);
        row["planta"].forEach(e => {
            $(`#${e}`).prop('checked', true);
        })
        artDT.rows(`#${id}`).remove().draw();
    }

    function save() {
        $.ajax({
            url: '{{url("articulos/edit")}}',
            // dataType: 'json',
            type: 'POST',
            data: {data: artDT.rows().data().toArray(), sol: {{$solicitud}},coment: document.getElementById("coment").value},
            success: (res) => {
                swal("Correcto!", "Articulos insertados correctamente", "success").then(function() {
                    window.location = '{{url("tickets")}}';
                });
            },
            error: (conf, text, error) => {
                swal("Oops!", "Problema al insertar", "error");
            }
        });
    }

</script>   
@stop