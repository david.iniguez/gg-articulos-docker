<!-- Modal -->
<div class="modal fade" id="modalArticulos" tabindex="-1" role="dialog" aria-labelledby="modalTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" style="max-width: 80%;" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" >Articulos de la solicitud</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div id="tablaC">
                    <table id="tablaArticulos" class="table table-sm table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>Tipo de articulo</th>
                                <th>Numero de parte</th>
                                <th>Aplicacion</th>
                                <th>Marca</th>
                                <th>Descripcion</th>
                                <th>Unidad de medida</th>
                                <th>Precio</th>
                                <th>Planta</th>
                            </tr>
                        </thead>
                        <tbody id="articulosTbody">
                        </tbody>
                    </table>
                </div>
            </div>

            <div id="foot" class="modal-footer">
                <div id="editButton"></div>
            </div>
        </div>
    </div>
</div>
