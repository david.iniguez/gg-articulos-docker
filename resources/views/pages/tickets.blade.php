@extends('adminlte::page')

@section('plugins.Datatables', true)

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <h5 class="card-header">Filtros de busqueda</h5>
                <div class="card-body">
                    <form id="form_filters" class="form" method="POST" action="{{ url('tickets/search') }}">
                        @csrf
                        <div class="form-group row">
                            <div class="form-group col-md-1"></div>

                            <div class="form-group col-md-2">
                                <label for="status">Estatus</label>
                                <select id="status" name="status" class="form-control">
                                    @if($stat != '0')
                                        <option value="{{$stat->Id}}"selected>{{$stat->Descripcion_2}}</option>
                                    @endif
                                    <option value="99">Todos</option>
                                    @foreach($estatus as $e)
                                    <option value="{{$e->Id}}">{{$e->Descripcion_2}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-md-4"></div>

                            <div class="form-group col-md-2">
                                <label for="fecha_ini">Fecha Inicio</label>
                                <input id="fecha_ini" type="date" class="form-control" name="fecha_ini" value="<?php echo date('Y-m-d'); ?>" required autocomplete="fecha_ini">
                            </div>
                            <div class="form-group col-md-2">
                                <label for="fecha_fin">Fecha Fin</label>
                                <input id="fecha_fin" type="date" class="form-control" name="fecha_fin" value="<?php echo date('Y-m-d'); ?>" requiredautocomplete="fecha_fin">
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <button type="submit" class="btn btn-primary">Filtrar</button>
                        </div>
                    </form>
                </div>
            </div>
            @if($typeUser == 1)
            <div class="card">
                <h5 class="card-header">Tickets</h5>
                    <div class="card-body">
                    <form id="tickets_form" method="POST" action="{{ url('tickets/validate') }}">
                        @csrf
                        <table id="ticketsDt" class="table table-sm table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th style="display:none;">ID</th>
                                    <th>Ticket</th>
                                    <th>Usuario</th>
                                    <th>Prioridad</th>
                                    <th>Tipo</th>
                                    <th>Fecha</th>
                                    <th>Estatus</th>
                                    <th style="text-align:center;">Detalles</th>
                                    <th style="text-align:center;">Descargar Excel</th>
                                    <th style="text-align:center;">Seleccionar</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($sols as $s)
                                    <tr style="{{($s->status == 'Pendiente')?"background-color:#bfbbfa":""}}">
                                        <td style="display:none;">{{$s->id}}</td>
                                        <td>{{$s->ticket}}</td>
                                        <td>{{$s->name}}</td>
                                        <td>{{$s->prioridad}}</td>
                                        <td>{{$s->tipo}} articulos</td>
                                        <td>{{date('d/m/Y', strtotime($s->fecha_creado))}}</td>
                                        <td>{{$s->status}}</td>
                                        <td align="center">
                                            <a href="#" data-toggle="modal" data-target="#modalArticulos" onclick="getArticulos('{{$s->id}}');">
                                                <i class="fas fa-info-circle"></i>
                                            </a>
                                        </td>
                                        <td align="center">
                                            <a href="#" onclick="descargar('{{$s->id}}')">
                                                <i class="fas fa-download"></i>
                                            </a>
                                        </td>
                                        @if ($s->status == 'Pendiente')
                                            <td align="center">
                                                <input type="checkbox" name="check" value="{{$s->id}}" >
                                            </td>
                                        @else
                                            <td align="center">
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                            <tbody>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th align="center">
                                    <div class="form-row">
                                        <button onClick="checkAll();" type="button" class="btn btn-md btn-secondary">Seleccionar todos</button>
                                    </div><br>
                                    <div class="form-row">
                                        <button onClick="sub();" class="btn btn-md btn-primary">Aprobar seleccionados</button>
                                    </div>
                                </th>
                            </tbody>
                        </table>
                    </form>
                    </div>
            </div>
            @else
            <div class="card">
                <h5 class="card-header">Tickets</h5>
                    <div class="card-body">
                        <table id="ticketsDt" class="table table-sm table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th style="display:none;">ID</th>
                                    <th>Ticket</th>
                                    <th>Prioridad</th>
                                    <th>Tipo</th>
                                    <th>Fecha</th>
                                    <th>Estatus</th>
                                    <th style="text-align:center;">Detalles</th>
                                    <th style="text-align:center;">Descargar Excel</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($sols as $s)
                                    <tr>
                                        <td style="display:none;">{{$s->id}}</td>
                                        <td>{{$s->ticket}}</td>
                                        <td>{{$s->prioridad}}</td>
                                        <td>{{$s->tipo}} articulos</td>
                                        <td>{{date('d/m/Y', strtotime($s->fecha_creado))}}</td>
                                        <td>{{$s->status}}</td>
                                        <td align="center">
                                            <a href="#" data-toggle="modal" data-target="#modalArticulos" onclick="getArticulos('{{$s->id}}');">
                                                <i class="fas fa-info-circle"></i>
                                            </a>
                                        </td>
                                        <td align="center">
                                            <a href="#" onclick="descargar('{{$s->id}}')">
                                                <i class="fas fa-download"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
            </div>
            @endif
        </div>
    </div>
</div>
@include('pages.modalArticulos')
@endsection

@section('js')
<script type="text/javascript">
    var datatable;

    $('#modalPlantSales').on('hidden.bs.modal', function () {
        $('#tablaArticulos'). DataTable ().destroy().draw ();
        $('#tablaArticulos').remove();
        $('#tablaC').append('<table id="tablaArticulos" class="table table-sm table-striped table-bordered" style="width:100%"><thead><tr><th>Tipo de articulo</th><th>Numero de parte</th><th>Aplicacion</th><th>Marca</th><th>Descripcion</th><th>Unidad de medida</th><th>Precio</th><th>Planta</th></tr></thead><tbody id="articulosTbody"></tbody></table>');
    });

    $(document).ready(function () {
        datatable = new $('#ticketsDt').DataTable({
            language: {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                "infoEmpty": "Mostrando 0 a 0 de 0 Entradas",
                "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Entradas",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        });
    });

    function descargar(id) {
        console.log(id);
        window.location="{{url('generarExcel/')}}/"+id;
    };

    function checkAll(){
      $("#tickets_form input[type='checkbox']").attr({
        "checked" : "checked"
      });
    };

    

    $('#tickets_form').submit( function() {
        var sData = fetchInput(datatable.$('input').serialize());
        console.log("datatable");

        $.each(sData, function(key, value) {
            $('#tickets_form').append("<input type='checkbox' style='display:none' name='selected[]' value='"+value+"' checked='checked' />");
        });
    } );

    function fetchInput(formData) {
        var form_data = formData.split('&');
        var input = {};

        $.each(form_data, function(key, value) {
            var data = value.split('=');
            input[key] = decodeURIComponent(data[1]);
        });

        return input;
    }

    
    function edit(idSol){
        window.location = `{{url("solicitud-articulos")}}/${idSol}`;
    }

    function getArticulos(idSol) {

        $('#articulosTbody').remove();
        $('#editButton').remove();
        $('#tablaArticulos').append('<tbody id="articulosTbody"></tbody>');
        $('#foot').append('<div id="editButton"></div>');

        var token = $('input[name="_token"]').val();

        $.ajax({
            url: '{{ url('articulos/modal') }}',
            method: "POST",
            data:{_token:token, idSol:idSol },
            success: function(result) {
                var data = JSON.parse(result);

                $.each(data, function(index, value) { 
                    if(value.num_parte == null){
                        value.num_parte = "";
                    }   
                    $('#articulosTbody').append('<tr><td>' + value.tipo_art + '</td><td>' + value.num_parte + '</td><td>' + value.aplicacion + '</td><td>' + value.marca + '</td><td>' + value.descripcion + '</td><td>' + value.unidad_medida +'</td><td align="right">$ ' + value.precio + '</td><td>' + value.planta + '</td></tr>');
                });

                $('#editButton').append('<button type="button" class="btn btn-primary" onClick="edit('+idSol+');">Editar</button>');

                $('#tablaArticulos').dataTable({
                    destroy: true,
                    "language": {
                        "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Spanish.json"
                    }
                });
            }
        });

}
    
</script>   
@stop