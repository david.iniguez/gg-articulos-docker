@extends('adminlte::page')

@section('plugins.Datatables', true)

@section('content')
<div class="container">
<form id="form_temp" name="form_temp" class="form">
    <div class="row justify-content-center">
         {{-- @csrf --}}
        <div class="col-md-12" id="div_1">
            <div class="card">
                <h5 class="card-header">Asistente para generar plantilla: </h5>
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-1"></div>

                        <div class="form-group col-md-5">
                            <label data-bs-toggle="tooltip" data-bs-placement="top" title="Campo Obligatorio.&#10;Prioridad que tendra el ticket en iTop">Prioridad</label><label style="color: blue;">*</label><br>
                            <input type="range" class="form-range" min="1" max="4" id="prioridad" name="prioridad" oninput="outputUpdate(value)" style="width: 300px;">
                            <output for="prioridad" id="val" for="prioridad">Alta</output>
                        </div>

                        <div class="form-group col-md-2"></div>

                        <div class="form-group col-md-2">
                            <label data-bs-toggle="tooltip" data-bs-placement="top" title="Campo Obligatorio.&#10;Alta de articulos, compartir en otras plantas o actualizar informacion">Tipo de ticket</label><label style="color: blue;">*</label>
                            <div class="form-check">
                                <input class="form-check-input" value="Alta" type="radio" name="tipo_tick" id="tipo_tick1">
                                <label class="form-check-label">Alta</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" value="Compartir" type="radio" name="tipo_tick" id="tipo_tick2">
                                <label class="form-check-label">Compartir</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" value="Actualizar" type="radio" name="tipo_tick" id="tipo_tick3">
                                <label class="form-check-label">Actualizar</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-10"></div>
                        <div class="form-group col-md-2">
                            <button class="btn btn-primary" type="button" onclick="next()">Siguiente</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12" id="div_2">
            <div class="card">
            <h5 class="card-header">Asistente para generar plantilla: <label id="tipoLabel"></label></h5>
            <div class="card-body">
                <div class="form-row">
                    <div class="form-group col-md-10"></div>
                    <div class="form-group col-md-2">
                        <label><label style="color: blue;">*</label> Campos obligatorios</label>
                    </div>
                </div>
            </div>
                <div class="card-body">
                    <div class="form-row">

                        <div class="form-group col-md-3" id="div_tipo_art">
                            <label data-bs-toggle="tooltip" data-bs-placement="top" title="Campo Obligatorio.&#10;Area de la cual es el articulo">Tipo de artículo</label><label style="color: blue;">*</label>
                            @foreach ($tipos_art as $item)
                                <div class="form-check">
                                    <input class="form-check-input" value='{{$item->id}}' type="radio" name="tipo_art" id="tipo_{{$item->id}}">
                                    <label class="form-check-label">{{$item->tipo}}</label>
                                </div>
                            @endforeach
                        </div>

                        <div class="form-group col-md-1"></div>

                        <div class="form-group col-md-4" id="div_aplicacion">
                            <label data-bs-toggle="tooltip" data-bs-placement="top" title="Campo Obligatorio.&#10;Para que se usa el articulo">Aplicación</label><label style="color: blue;">*</label>
                            <select class="form-control" id="aplicacion" name="aplicacion"></select>
                        </div>

                        <div class="form-group col-md-4" id="div_marca">
                            <label data-bs-toggle="tooltip" data-bs-placement="top" title="Campo Obligatorio.&#10;Marca del articulo">Marca</label><label style="color: blue;">*</label>
                            <select class="form-control" id="marcas" name="marcas"></select>
                        </div>
                    </div>

                    <div class="form-row">

                        <div class="form-group col-md-3" id="div_num_part">
                            <label data-bs-toggle="tooltip" data-bs-placement="top" title="Campo opcional.&#10;Numero de parte del articulo">Número de parte</label>
                            <input class="form-control" type="text" id="num_parte" name="num_parte">
                        </div>

                        <div class="form-group col-md-1"></div>
                        
                        <div class="form-group col-md-3" id="div_art_oracle">
                            <label data-bs-toggle="tooltip" data-bs-placement="top" title="Campo Obligatorio.&#10;Código del articulo en Oracle">Artículo Oracle</label><label style="color: blue;">*</label>
                            <input class="form-control" type="text" id="art_oc" name="art_oc">
                        </div>

                        <div class="form-group col-md-8">
                            <label data-bs-toggle="tooltip" data-bs-placement="top" title="Campo Obligatorio.&#10;Descripcion del articulo">Descripción del artículo</label><label style="color: blue;">*</label>
                            <textarea class="form-control" type="text" id="desc" name="desc"></textarea>
                        </div>
                    </div>

                    <div class="form-row">

                        <div class="form-group col-md-3" id="div_u_medida">
                            <label data-bs-toggle="tooltip" data-bs-placement="top" title="Campo Obligatorio.&#10;Unidad de medida del articulo">Unidad de medida</label><label style="color: blue;">*</label>
                            <select class="form-control" id="medida" name="medida">
                                <option value="" selected>Selecciona la unidad de medida</option>
                                @foreach($unidades_medida as $um)
                                    <option value='{{$um->id}}'>{{$um->unidad_medida}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-1"></div>

                        <div class="form-group col-md-2">
                            <label data-bs-toggle="tooltip" data-bs-placement="top" title="Campo Obligatorio.&#10;Precio del articulo">Precio</label><label style="color: blue;">*</label>
                            <div class="input-group mb-3">
                                <span class="input-group-text">$</span><input class="form-control" type="decimal" id="precio" name="precio" min="1" >
                            </div>
                        </div>

                        <div class="form-group col-md-7"></div>
                        
                        
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12" id="div_plantas">
                            <label>Plantas</label><label style="color: blue;">*</label>
                        </div>
                        <div class="form-group col-md-6 plt">
                            <label class="text-secondary">Plantas PRESER</label>
                            <select class="select" id="planta_preser" name="planta_preser" multiple>
                                @foreach ($plantas_p as $planta)
                                    <option value="{{$planta->id}}">{{$planta->Codigo}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6 plt">
                            <label class="text-secondary">Plantas SEEMEX</label>
                            <select class="select" id="planta_seemex" name="planta_seemex" multiple>
                                @foreach ($plantas_s as $planta)
                                    <option value="{{$planta->id}}">{{$planta->Codigo}}</option>
                                @endforeach
                            </select>
                        </div>
                        @foreach ($plantas as $planta)
                            <div class="form-group col-md-3 plt">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="{{$planta->id}}" name="planta_{{$planta->id}}" id="{{$planta->id}}">
                                    <label class="form-check-label" id="planta_{{$planta->id}}">{{$planta->Codigo}}</label>
                                </div>
                            </div>
                        @endforeach
                        <div class="form-group col-md-3 plt">
                            {{-- Cambiar por un boton --}}
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="{{$razon}}" name="planta_{{$razon}}" id="{{$razon}}">
                                <label class="form-check-label" id="planta_{{$razon}}">Todas</label>
                            </div>
                        </div>
                        <div class="form-group col-md-3" id="todas_plt">
                            <div class="form-check">
                                <label class="text-secondary" id="planta_666">Todas las plantas seleccionadas</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-10"></div>
                        <div class="form-group col-md-2">
                            {{-- <button type="submit" class="btn btn-primary">Generar</button> --}}
                            <button class="btn btn-primary" type="button" onclick="addElement()">Agregar</button>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</form>
<form id="form_2" name="form_2" class="form" method="post" enctype="multipart/form-data" action="{{url('file/insert')}}">
    @csrf
    <div class="card" id="div_adjunto">
        <div class="card-body">
            <div class="form-row">
                <div class="form-group col-md-3">
        			<label class="form-label">Seleccionar archivo </label>
    		    </div>
		    
		        <div class="form-group col-md-1"></div>
		    
    		    <div class="form-group col-md-1">
            	    <input type="file" title="seleccionar fichero" id="importData" />
            	</div>
            </div>
            <div id="archivos_selecc"></div>
            
            <div class="form-group col-md-8">
                <label>Comentarios</label>
                <textarea class="form-control" type="text" id="coment" name="coment"></textarea>
            </div>
        </div>
    </div>
</form>
<div class="card" id="div_3">
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                <table class="table table-sm table-striped table-bordered" id="articlesDT" class="display">
                </table>
                <button id="generar" class="btn btn-primary" onclick="save()">Generar</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal_art" tabindex="-1" role="dialog" aria-labelledby="modalTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="form-group col-md-6">
                    <h5 class="modal-title">Articulos semejantes</h5>
                </div>
                <div class="form-group col-md-4"></div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-sm table-striped table-bordered" id="t-art" style="width:100%">
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')  
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">

    let idA = 0;
    let artDT;
    let matchDT;
    let temporal;
    
    document.getElementById('importData').addEventListener('change', handle_files, false);
    
    function handle_files(evt){
        //copy es la copia del input del archivo
        var copy = evt.target.cloneNode();
        copy.name = 'archivo';
        copy.id = '';
        copy.hidden = true;
        var div = document.createElement('div');
        var button = document.createElement('button');
        button.innerHTML = copy.files[0].name;
        button.onclick = (e) => {e.target.parentNode.remove()}
        div.appendChild(button);
        div.appendChild(copy);
        document.getElementById('archivos_selecc').appendChild(div);
    }

    $('#modal_art').on('hidden.bs.modal', function () {
        $('#matchDT').DataTable().destroy().draw();
        $('#matchDT').remove();
    });

    function outputUpdate(val){
        if(val == 1){
            document.querySelector('#val').value = "Baja";
        }else if(val == 2){
            document.querySelector('#val').value = "Media";
        }else if(val == 3){
            document.querySelector('#val').value = "Alta";
        }else if(val == 4){
            document.querySelector('#val').value = "Crítica";
        }
    }

    $(document).ready(function () {
        $("#div_2").hide();
        $("#div_3").hide();
        $("#div_adjunto").hide();
        $("#planta_preser").select2();
        $("#planta_seemex").select2();

        matchDT = new $('#t-art').DataTable({
            data: [],
            columns: [
                {
                    title: 'Código de artículo',
                    width: '20%'
                },
                {title: 'Descripción'}
            ]
        });
    });

    function openM(arts) {
        $("#modal_art").modal("show");
        arts = artDT.row(arts).data()[11][1];
        
        matchDT.clear().draw();
        
        matchDT.rows.add(arts);

        matchDT.columns.adjust().draw();
        
    }

    function addElement() {
        let element = {};
        element.planta = [];

        $("#form_temp").serializeArray().forEach(e => {
            if (e.name.includes("planta")) {
                element.planta.push(e.value);
            }else {
                element[e.name] = e.value;
            }
        });
        
        let errores = '';
        let hasError = false;
    
        if (element.tipo_tick=="Alta") {
            element.planta.push("666")
            
            if (element.desc=='') {
                errores += 'Ingrese una descripción\n';
                hasError = true;
            }
            if (element.aplicacion==undefined) {
                errores += 'Ingrese una aplicacion\n';
                hasError = true;
            }
            if (element.marcas==undefined) {
                errores += 'Ingrese una marca\n';
                hasError = true;
            }
            if (element.medida==''||element.medida==undefined) {
                errores += 'Seleccione una unidad de medida\n';
                hasError = true;
            }
            if (element.tipo_art=='3'&&element.num_parte=="") {
                errores += 'Seleccione un numero de parte\n';
                hasError = true;
            }
        } else{
            if(element.art_oc==''){
                errores += 'Ingrese el código del articulo en Oracle\n';
                hasError = true;
            }
        }
        
        if (element.tipo_art==undefined) {
            errores += 'Seleccione un tipo de articulo\n';
            hasError = true;
        }
        if (element.desc=='') {
                errores += 'Ingrese una descripción\n';
                hasError = true;
            }
        if (!(/(^[0-9]+(.[0-9]{0,2})?$)/.test(element.precio))) {
            errores += 'Ingrese un precio valido\n';
            hasError = true;
        }
        if (element.planta.length==0) {
            errores += 'Seleccione al menos una planta\n';
            hasError = true;
        }

        if (hasError) {
            swal("Oops!", errores, "error");
            return;
        }
        
        let art = [];
        //TODO: Agregar Validaciones para campos obligatorios
        art.push(element.prioridad);
        art.push(element.tipo_tick);
        art.push(element.tipo_art);
        art.push(element.aplicacion??0);
        art.push(element.marcas??0);
        art.push(element.num_parte);
        art.push(element.desc);
        art.push(element.medida??'');
        art.push(element.precio);
        art.push(element.planta);
        art.push(element.art_oc);
        let id = idA;

        artDT.row.add(
            art.concat([
                [id, []],
                id,
                id
            ])
        ).node().id = id;
        $('input:checkbox').prop('checked', false);
        $('#num_parte').val('');
        $('#desc').val('');
        $('#medida').val('Selecciona la unidad de medida');
        $('#precio').val('');
        $('#aplicacion').val(null).trigger('change');
        $('#marcas').val(null).trigger('change');
        $('#art_oc').val('');
        
        artDT.draw();
        if (element.tipo_tick=="Alta"){
            $.ajax({
                url: '{{url("articulos/get")}}',
                dataType: 'json',
                type: 'POST',
                data: {
                    num_parte: element.num_parte,
                    desc: element.desc
                },
                success: (res) => {
                    if (res.length==0) {
                        return;
                    }
                    artDT.cell(artDT.row(`#${id}`).index(), 11).data([id, res]);
                    openM(id)
                },
                error: (conf, text, error) => {
                    console.log("Error:", text);
                }
            });
        }
        
        idA = idA+1;
    }

    function deleteRow(row) {
        artDT.rows(`#${row}`).remove().draw();
    }

    function editData(row) {
        let id = row;
        row = artDT.row(`#${row}`).data();
        document.form_temp.tipo_art.value=row[2];
        $('#aplicacion').val(row[3]).trigger("change");
        $('#marcas').val(row[4]).trigger("change");
        $('#num_parte').val(row[5]);
        $('#desc').val(row[6]);
        $('#medida').val(row[7]);
        $('#precio').val(row[8]);
        row[9].forEach(e => {
            $(`#${e}`).prop('checked', true);
        })
        $('#art_oc').val(row[10]);
        artDT.rows(`#${id}`).remove().draw();
    }

    function next(){
        tipoTicket = "Vacio";
        const rbs = document.querySelectorAll('input[name="tipo_tick"]');
        let selectedValue;
        for (const rb of rbs) {
            if (rb.checked) {
                tipoTicket = rb.value;
                break;
            }
        }

        if(tipoTicket != "Alta" && tipoTicket != "Compartir" && tipoTicket != "Actualizar"){
            swal("Oops!", "Ingrese un tipo de ticket", "error");
        }else{
            $("#div_2").show();
            $("#div_3").show();
            $("#div_1").hide();
            $("#div_adjunto").show();

            
            $("#tipoLabel").text(tipoTicket+" articulos");

            if($("#form_temp").serializeArray()[1].value!="Alta"){
                $("#todas_plt").hide();
                $("#div_num_part").hide();
                $("#div_marca").hide();
                $("#div_u_medida").hide();
                $("#div_aplicacion").hide();
                
                
            } else {
                $(".plt").hide();
                $("#div_art_oracle").hide();
            }
            
            fetch("{{url('select2-marcas')}}",{
                method: 'GET'
            }).then(r=>r.json())
            .then(d => {
                $('#marcas').select2({
                    data:d,
                    tags: true
                });
                $('#marcas').val(null).trigger('change');
            }).catch(err => {
                console.log(err);
            });
            
            fetch("{{url('select2-aplicacion')}}",{
                method: 'GET'
            }).then(r=>r.json())
            .then(d => {
                $('#aplicacion').select2({
                    tags: true,
                    data:d
                });
                $('#aplicacion').val(null).trigger('change');
            }).catch(err => {
                console.log(err);
            })
            let hidden = [];
            if(tipoTicket == "Alta"){
                hidden = [0,1];
            }else{
                hidden = [0,1,3,4,5,7,11];
            }
            
        $('#articlesDT').DataTable({
            data: [],
            columnDefs: [
                {
                    orderable: false,
                    targets: [11, 12, 13]
                },
                /// Hide Columns
                {
                    targets: hidden,
                    visible: false,
                    searchable: false
                }
            ],
            language: {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                "infoEmpty": "Mostrando 0 a 0 de 0 Entradas",
                "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Entradas",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            },
            columns: [
                { title: "Prioridad" },
                { title: "Tipo Ticket" },
                { 
                    title: "Tipo Articulo",
                    render: function (data, type, full, meta) {
                        return $(`#tipo_${data}`).next().text();
                    }
                },
                { 
                    title: "Aplicacion",
                    render: function (data, type, full, meta) {
                        return data == ''?'':$("#aplicacion").find(`option[value='${data}']`).text();
                    }
                },
                {
                    title: "Marca",
                    render: function (data, type, full, meta) {
                        return data == ''?'':$("#marcas").find(`option[value='${data}']`).text();
                    }
                },
                { title: "Num Parte" },
                { title: "Desc" },
                { 
                    title: "Medida",
                    render: function (data, type, full, meta) {
                        return data == ''?'':document.getElementById("medida").options[data].text;
                    }
                },
                {
                    title: "Precio",
                    render: function (data, type, full, meta) {
                        return `$${parseFloat(data).toFixed(2)}`;
                    } 
                },
                { 
                    title: "Planta",
                    //TODO: Si es un array agregar la opcion de mostrar lista al poner el puntero Array.isArray(data)
                    render: function (data, type, full, meta) {
                        return data.length>1 
                        ? `${data.length} plantas`
                        // : (data[0]??1)?.value??'';
                        : $(`#planta_${data[0]}`).text();
                    } 
                },
                {
                    title: "Articulo Oracle"
                },
                { 
                    title: "Existe",
                    render: (data, type, full, meta) => {
                        return data[1].length>0?`<a onclick="openM(${data[0]})">
                            <i class="fas fa-exclamation-triangle" style="color: Tomato;">
                        </a>`:'';
                    },
                },
                { 
                    title: "",
                    render: function (data, type, full, meta) {
                        return `<div class="mb-3"><button type="button" class="btn btn-info btn-sm" onclick="editData(${data})">Editar</button><br></div>`;
                    } 
                },
                { 
                    title: "",
                    render: function (data, type, full, meta) {
                        return `<div class="mb-3"><button type="button" class="btn btn-danger btn-sm" onclick="deleteRow(${data})">Eliminar</button><br></div>`;
                    }
                    
                }
            ]
        });
        artDT = $('#articlesDT').DataTable();
        }
        
    }

    function save() {
        if(artDT.rows().count()>0){
            $.ajax({
                url: '{{url("articulos/insert")}}',
                // dataType: 'json',
                type: 'POST',
                data: {data: artDT.rows().data().toArray(),coment: document.getElementById("coment").value},
                success: (res) => {
                    upload_files(res);
                },
                error: (conf, text, error) => {
                    swal("Oops!", "Problema al insertar", "error");
                }
            });
        }else{
            alert('Agrega al menos un articulo');
        }
    }
    
    function upload_files(id) {
        let formData = new FormData()
        let lista_input = document.getElementsByName('archivo')
        for (let i = 0; i<lista_input.length; i++){
            formData.append("archivo[]",lista_input[i].files[0]);
            console.log("Agregando: ",lista_input[i].files[0]);
        }
        formData.append("id",id)
        
        var token = $('input[name="_token"]').val();
        
        fetch("{{url('file/insert')}}",{
            method: 'POST',
            body: formData
        }).then(response => {
            console.log(response)
            swal("Correcto!", "Articulos insertados correctamente", "success").then(function() {
                // window.location = '{{url("template")}}';
            });
        }).catch(err => {
            console.log(err)
            swal("Correcto!", "Articulos insertados correctamente\nProblema al subir archivos", "success").then(function() {
                // window.location = '{{url("template")}}';
            });
        })
        
        
    
    }

    /// get selected rows: $('#articlesDT').DataTable().$('input[type="checkbox"]:checked')
</script>   
@stop