@extends('adminlte::page')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Menu Principal</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if($type_user == '1' && $estatus_sol != 0)
                        <label for="sol_pend" style="color: #f45050">Tienes solicitudes pendientes por aprobar</label>
                    @else
                        <label for="sol_pend">Bienvenid@</label>
                    @endif
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
