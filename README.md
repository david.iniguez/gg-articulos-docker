# Proyecto GG Articulos - Powered By: Drumbot
## Instalar dependencias con docker
`docker run --rm     -u "$(id -u):$(id -g)"     -v $(pwd):/opt     -w /opt     laravelsail/php80-composer:latest     composer install --ignore-platform-reqs`
`./vendor/bin/sail up -d`
## Preparar el Storage
Si se está usando docker:  
`./vendor/bin/sail artisan storage:link`  
De lo contrario:  
`artisan storage:link`  
