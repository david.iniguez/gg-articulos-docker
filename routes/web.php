<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\Pages\TemplateController;
use App\Http\Controllers\Pages\TicketsController;
use App\Http\Controllers\Endpoints\EndPointsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('home', [HomeController::class, 'index'])->name('home');

// Authentication Routes...
Route::get('login', [LoginController::class, 'showLoginForm'])->name('login');
Route::post('login', [LoginController::class, 'login']);
Route::post('logout', [LoginController::class, 'logout'])->name('logout');

// Registration Routes...
Route::get('register', [RegisterController::class,'form'])->name('register');
Route::post('register', [RegisterController::class,'insert']);

// Password Reset Routes...
Route::get('password/reset', [ForgotPasswordController::class,'showLinkRequestForm'])->name('password.request');
Route::post('password/email', [ForgotPasswordController::class,'sendResetLinkEmail'])->name('password.email');
Route::get('password/reset/{token}', [ResetPasswordController::class,'showResetForm'])->name('password.reset');
Route::post('password/reset', [ResetPasswordController::class,'reset'])->name('password.update');

// Email Verification Routes...
Route::emailVerification();

/*******TEMPLATE START*******/
///VIEWS
Route::get('template', [TemplateController::class,'form'])->name('template');
Route::get('select2-marcas',[TemplateController::class,'select2_marcas']);
Route::get('select2-aplicacion',[TemplateController::class,'select2_aplicacion']);
///END POINTS
Route::post('articulos/get', [TemplateController::class,'search']);
Route::post('articulos/edit', [TemplateController::class,'edit']);
Route::post('articulos/insert', [TemplateController::class,'insert']);
Route::get('generarExcel/{id}', [TemplateController::class,'excel']);
Route::post('file/insert', [TemplateController::class,'insertFile']);
Route::get('file/get/{idSol}', [TemplateController::class,'getFiles']);
Route::get('file/download/{id}/{file}', [TemplateController::class,'downloadFile']);
/*******TEMPLATE END*******/

/*******TICKETS START*******/
Route::get('tickets', [TicketsController::class,'form'])->name('tickets');
Route::post('tickets/search',[TicketsController::class,'search']);
Route::post('tickets/validate',[TicketsController::class,'aprobar']);
Route::post('articulos/modal',[TicketsController::class,'detail']);
Route::get('solicitud-articulos/{solicitud}',[TemplateController::class,'edit_arts']);
Route::get('articulos-get/{solicitud}',[TemplateController::class,'getArticulos']);
/*******TICKETS END*******/

//END POINTS
Route::get('ticketsPendientes/{tipo}', [EndPointsController::class,'ticketsPendientes']);
Route::get('ticketsAplicados', [EndPointsController::class,'ticketsAplicados']);
Route::get('codigos/{group?}', [EndPointsController::class,'getCodigos']);
Route::get('articulosError/{ticket}', [EndPointsController::class,'articulosError']);
Route::get('articulosCorrectos/{ticket}', [EndPointsController::class,'articulosCorrectos']);
Route::get('revisarCodigo/{codigo}',[EndPointsController::class,'revisarUnidad']);
Route::get('solicitudesPendientes',[EndPointsController::class,'solicitudesPendientes']);
Route::get('articulosxSolicitud/{idSol}',[EndPointsController::class,'articulosxSolicitud']);
Route::get('tipoSolicitud',[EndPointsController::class,'tipoSolicitud']);
Route::get('updateEstatus/{num_ticket}/{idSol}/{stat}',[EndPointsController::class,'updateEstatus']);
Route::get('updateEstatusArt/{id}/{estatus}/{coment}',[EndPointsController::class,'updateEstatusArt']);
Route::get('obtenerMarcasPendientes',[EndPointsController::class,'obtenerMarcasPendientes']);

Route::post('insertTicket', [EndPointsController::class,'insertTicket']);
Route::post('insertArticulo', [EndPointsController::class,'insertArticulo']);
Route::post('insertArticuloCat', [EndPointsController::class,'insertArticuloCat']);
Route::post('insertMarcaCat', [EndPointsController::class,'insertMarcaCat']);
Route::post('updateEstatusMarca', [EndPointsController::class,'updateEstatusMarca']);

Route::get('actualizarTicket/{num}/{stat}', [EndPointsController::class,'updateTicket']);
Route::get('actualizarArticulo/{id}/{stat}/{error?}', [EndPointsController::class,'updateArticulo']);

