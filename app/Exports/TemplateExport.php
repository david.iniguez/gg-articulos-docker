<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;

class TemplateExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    protected $id;

    public function __construct($id) {
            $this->id = $id;
    }

    public function headings(): array
    {
        return [
            'iTop',
            'TIPO_TICKET',
            'TIPO_ARTICULO',
            'INVENTARIABLE',//
            'CLASE',//
            'CATEGORIA',//
            'ITEM',//
            'CONCATENA',//
            'SISTEMA',//
            'DESCRIPCION',
            'APLICACION',
            'CODIGO',
            'MARCA',
            'U/M',
            'PRECIO',
            'PLANTA'
        ];
    }

    public function collection()
    {
        $solicitud = DB::table('solicitud_articulos2')
            ->where('id',$this->id)
        ->get();
        
        return $solicitud;
    }
}
