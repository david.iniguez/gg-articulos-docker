<?php

namespace App\Http\Controllers\Endpoints;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class EndpointsController extends Controller
{
    public function getCodigos($group = null){
        if(is_null($group) or $group == "TODAS"){
            $codigos = DB::table('Unidades_Negocio')->where('Planta',1)->get('Codigo');
        }else if($group == "ALL"){
            $codigos = DB::table('Unidades_Negocio')->get('Codigo');
        }else if($group == "TODAS CORAGAS"){
            $codigos = DB::table('Unidades_Negocio')
                ->where([
                    ['Razon_Social',"CORAGAS SA DE CV"],
                    ['Planta',1]
                ])
            ->get('Codigo');
        }else if($group == "TODAS GAS GLOBAL"){
            $codigos = DB::table('Unidades_Negocio')
                ->where([
                    ['Razon_Social',"GAS GLOBAL CORPORATIVO SA DE CV"],
                    ['Planta',1]
                ])
            ->get('Codigo');
        }else if($group == "TODAS GAS MENGUC"){
            $codigos = DB::table('Unidades_Negocio')
                ->where([
                    ['Razon_Social',"GAS MENGUC SA DE CV"],
                    ['Planta',1]
                ])
            ->get('Codigo');
        }else if($group == "TODAS SERVIGAS"){
            $codigos = DB::table('Unidades_Negocio')
                ->where([
                    ['Razon_Social',"SERVIGAS SA DE CV"],
                    ['Planta',1]
                ])
            ->get('Codigo');
        }else if($group == "TODAS GG GAS"){
            $codigos = DB::table('Unidades_Negocio')
                ->where([
                    ['Razon_Social',"GG GAS"],
                    ['Planta',1]
                ])
            ->get('Codigo');
        }else if($group == "TODAS GAS UNO"){
            $codigos = DB::table('Unidades_Negocio')
                ->where([
                    ['Razon_Social',"GAS UNO"],
                    ['Planta',1]
                ])
            ->get('Codigo');
        }else if($group == "TODAS RAMAGAS"){
            $codigos = DB::table('Unidades_Negocio')
                ->where([
                    ['Razon_Social',"RAMAGAS"],
                    ['Planta',1]
                ])
            ->get('Codigo');
        }else if($group == "TODAS MINAGAS"){
            $codigos = DB::table('Unidades_Negocio')
                ->where([
                    ['Razon_Social',"MINAGAS"],
                    ['Planta',1]
                ])
            ->get('Codigo');
        }else if($group == "TODO CH4"){
            $codigos = DB::table('Unidades_Negocio')
                ->where([
                    ['Razon_Social',"CH4"],
                    ['Planta',1]
                ])
            ->get('Codigo');
        }else{
            $codigos = "No definido el agrupador ". $group;
        }
      
        return $codigos;
    }

    public function ticketsPendientes($tipo){

        $ticketsPend = DB::table('Tickets')
            ->join('Articulos','Articulos.Num_Ticket','Tickets.Numero')
            ->select('Articulos.*')
            ->where([
                ['Articulos.Tipo',$tipo],
                ['Tickets.Estatus',1],
                ['Articulos.Estatus',1]
            ])
        ->get();

        return $ticketsPend;
    }

    public function articulosError($ticket){

        $articulosError = DB::table('Articulos')
            ->select(
                'Numero',
                'Descripcion',
                'Error'
            )
            ->where([
                ['Num_Ticket',$ticket],
                ['Estatus',2]
            ])
        ->get();

        return $articulosError;
    }

    public function articulosCorrectos($ticket){

        $articulosC = DB::table('Articulos')
            ->join('Tickets','Tickets.Numero','Articulos.Num_Ticket','Articulos.Error')
            ->select(
                'Articulos.Numero',
                'Articulos.Descripcion',
                'Articulos.Tipo',
                'Articulos.Error'
            )
            ->where([
                ['Articulos.Num_Ticket',$ticket],
                ['Articulos.Estatus',3]
            ])
        ->get();

        return $articulosC;
    }

    public function tipoSolicitud(){
        $tipoS = DB::table('tickets_v')->get();

        return json_encode($tipoS);
    }

    public function ticketsAplicados(){
        $ticketsAp = DB::table('Tickets')
            ->where('Estatus',3)
        ->get('Numero');

        return $ticketsAp;
    }

    public function revisarUnidad($codigoUnidad){
        $valido = DB::table('Unidades_Negocio')->where('Codigo',$codigoUnidad)->value('Codigo');

        if(is_null($valido)){
            return "El codigo ".$codigoUnidad." no es un codigo de unidad de negocio valido.";
        }else{
            return 1;
        }
    }

    public function solicitudesPendientes(){

        $sol = DB::table('solicitudes')->where('estatus',2)->get();

        if(is_null($sol)){
            return 0;
        }else{
            return 1;
        }
    }

    public function articulosxSolicitud($idSol){

        $art = DB::table('solicitud_articulos')->where('id',$idSol)->get();

        return json_encode($art, JSON_UNESCAPED_UNICODE);
    }

    public function insertTicket(Request $r){

        DB::table('Tickets')
        ->insert([
            'Numero' => $r->Numero
        ]);
    }
    
    public function obtenerMarcasPendientes(){
        return DB::table('cat_marcas')
            ->where('registrado','=',0)
        ->get();
    }

    public function insertArticulo(Request $r){

        $existente = DB::table('Articulos')
            ->where([
                ['Numero', $r->Numero],
                ['Num_Ticket', $r->Num_Ticket],
                ['Tipo', $r->Tipo],
                ['Planta', $r->Planta],
                ['Codigo',$r->Codigo],
                ['Marca',$r->Marca],
                ['Precio',$r->Precio]
            ])
        ->get();
        if(sizeof($existente) == 0){
             if($r->Tipo == "Ya Existente"){
                 DB::table('Articulos')
                ->insert([
                    'Numero' => $r->Numero,
                    'Num_Ticket' => $r->Num_Ticket,
                    'Tipo' => $r->Tipo,
                    'Inventariable' => $r->Inventariable,
                    'Clase' => $r->Clase,
                    'Categoria' => $r->Categoria,
                    'Concatena' => $r->Concatena,
                    'Sistema' => $r->Sistema,
                    'Descripcion' => $r->Descripcion,
                    'Aplicacion' => $r->Aplicacion,
                    'Codigo' => $r->Codigo,
                    'Marca' => $r->Marca,
                    'Unidad_Medida' => $r->Unidad_Medida,
                    'Precio' => $r->Precio,
                    'Planta' => $r->Planta,
                    'Estatus' => 3
                ]);
             }
             else if($r->Tipo == "No Viable"){
                 DB::table('Articulos')
                ->insert([
                    'Numero' => $r->Numero,
                    'Num_Ticket' => $r->Num_Ticket,
                    'Tipo' => $r->Tipo,
                    'Inventariable' => $r->Inventariable,
                    'Clase' => $r->Clase,
                    'Categoria' => $r->Categoria,
                    'Concatena' => $r->Concatena,
                    'Sistema' => $r->Sistema,
                    'Descripcion' => $r->Descripcion,
                    'Aplicacion' => $r->Aplicacion,
                    'Codigo' => $r->Codigo,
                    'Marca' => $r->Marca,
                    'Unidad_Medida' => $r->Unidad_Medida,
                    'Precio' => $r->Precio,
                    'Planta' => $r->Planta,
                    'Estatus' => 3,
                    'Error' => $r->Comentario
                ]);
             }
             else{
                 DB::table('Articulos')
                ->insert([
                    'Numero' => $r->Numero,
                    'Num_Ticket' => $r->Num_Ticket,
                    'Tipo' => $r->Tipo,
                    'Inventariable' => $r->Inventariable,
                    'Clase' => $r->Clase,
                    'Categoria' => $r->Categoria,
                    'Concatena' => $r->Concatena,
                    'Sistema' => $r->Sistema,
                    'Descripcion' => $r->Descripcion,
                    'Aplicacion' => $r->Aplicacion,
                    'Codigo' => $r->Codigo,
                    'Marca' => $r->Marca,
                    'Unidad_Medida' => $r->Unidad_Medida,
                    'Precio' => $r->Precio,
                    'Planta' => $r->Planta,
                ]);
             }
                 
            return "Nuevo";
        }else{
            
            if($existente[0]->Estatus == "2"){
                DB::table('Articulos')
                ->where([
                    ['Numero',$r->Numero],
                    ['Num_Ticket', $r->Num_Ticket],
                    ['Tipo', $r->Tipo],
                ])
                ->update([
                    'Tipo' => $r->Tipo,
                    'Inventariable' => $r->Inventariable,
                    'Clase' => $r->Clase,
                    'Categoria' => $r->Categoria,
                    'Concatena' => $r->Concatena,
                    'Sistema' => $r->Sistema,
                    'Descripcion' => $r->Descripcion,
                    'Aplicacion' => $r->Aplicacion,
                    'Codigo' => $r->Codigo,
                    'Marca' => $r->Marca,
                    'Unidad_Medida' => $r->Unidad_Medida,
                    'Precio' => $r->Precio,
                    'Planta' => $r->Planta,
                    'Estatus' => 1,
                    'Error' => NULL
                ]);
                
                $this->updateTicket($existente[0]->Num_Ticket,1);
                return "Existe y es error";
            }else{
                return "Existe";
            }
            
        }
    }

    public function insertArticuloCat(Request $r){

        DB::table('cat_articulos')
        ->insert([
            'Item' => $r->item,
            'Clase' => $r->clase,
            'Categoria' => $r->cat,
            'Descripcion' => $r->desc,
            'Precio' => $r->precio,
            'UnidadMedida' => $r->um,
            'Marca' => $r->marca
        ]);
    }

    public function insertMarcaCat(Request $r){
        
        $marca = DB::table('cat_marcas')->where('marca',$r->marca)->value('id');

        if(is_null($marca)){
            DB::table('cat_marcas')
            ->insert([
                'marca' => $r->marca
            ]);
        }
    }

    public function updateTicket($numero, $status){
        
        $date = date('Y-m-d');
        
        if($status == 3){
            DB::table('Tickets')
            ->where('Numero',$numero)
            ->update([
                'Estatus' => $status,
                'Fecha_Ap' => $date
            ]);

            DB::table('Articulos')
            ->where([
                ['Num_Ticket',$numero],
                ['Estatus', 1]
            ])
            ->update([
                'Estatus' => $status
            ]);
        }else{
            DB::table('Tickets')
            ->where('Numero',$numero)
            ->update([
                'Estatus' => $status
            ]);
        }
        
    }

    public function updateArticulo($id, $status, $error){

        if($status == 2){
            DB::table('Articulos')
            ->where([
                ['Numero',$id],
                ['Error',NULL]
            ])
            ->update([
                'Estatus' => $status,
                'Error' => $error
            ]);

            $numTicket = DB::table('Articulos')
                ->where('Numero',$id)
            ->value('Num_Ticket');

            $this->updateTicket($numTicket,3);
        }else{
            DB::table('Articulos')
            ->where('Numero',$id)
            ->update([
                'Estatus' => $status
            ]);
        }
        
    }

    public function updateEstatus($numeroTicket, $idSol, $stat){

        DB::table('solicitudes')
        ->where('id',$idSol)
        ->update([
            'Estatus' => $stat,
            'ticket' => $numeroTicket
        ]);
   
    }
    
    public function updateEstatusMarca(Request $req){
        if(is_null($req->id)){
            echo 'es nulo';
            DB::table('cat_marcas')
                ->where('marca', $req->marca_base)
            ->update(['registrado'=>0, 'marca'=>$req->marca_ok]);
        }else{
             DB::table('cat_marcas')
                ->where('id', $req->id)
            ->update(['registrado'=>1]);
            echo 'no es nulo';
        }
        
    }
    
}
