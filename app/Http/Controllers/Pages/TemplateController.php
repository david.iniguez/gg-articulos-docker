<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Exports\TemplateExport;
use Excel;

class TNode {
    /**
     * @var TNode::array
     * Children of current node
     */
    public $children;

    /**
     * @var bool
     * Flag to determine if is an End Of Word or a leaf node
     */
    public $isEOW;


    public function __construct() {
        $this->isEOW = false;
        $this->children = array();
    }
}

class TrieDS {

    /**
     * @var TNode
     * Root node
     */
    public $root;
    
    
    public function __construct() {
        $this->root = new Tnode();
    }

    /**
     * @param string $words Key to be inserted
     */
    public function insert($words) {
        foreach(explode(" ", $words) as $word){
            $act = $this->root;
            foreach (str_split($word) as $key) {
                if (!array_key_exists($key, (array) $act->children)) {
                    $act->children[$key] = new Tnode();
                }
                $act = $act->children[$key];
            }
        }
        $act->isEOW = true;
    }

    /**
     * @param string $words Words to search
     * @param int $margin Minimum of words to match
     */
    public function find($words, $margin, $percentage) {
        /**
         * @var int
         * Number of jumps do
         */
        $similarity = 0;
        foreach(explode(" ", $words) as $word){
            $act = $this->root;
            $level = 0;
            if (strlen($word)<=2) {
                continue;
            }
            foreach (str_split($word) as $key) {
                if (!array_key_exists($key, (array) $act->children)) {
                    continue;
                }
                // echo "KEY: $key, LEVEL: $level, P: <br>";
                $level++;
                $act = $act->children[$key];
            }
            // echo "WORD: $word, LEVEL: $level, P: ".($level*100/strlen($word))."<br>";
            if (($level*100/strlen($word))>$percentage) {
                $similarity += 1;
            }
        }

        return $similarity>=$margin;
    }

    public function print($node, $level = 0){
        foreach ($node->children as $k => $child) {
            if (!$child->isEOW) {
                echo "K: $k, level: $level<br>";
                $this->print($child, $level+1);
            }else {
                echo "C: $k;<br>";
            }
        }
    }

}

class TemplateController extends Controller {
    public function form(){
        $userId =  Auth::user()->id;
        $empresasAsociadas = DB::table('usuarios_empresas')->where('idUser',$userId)->pluck('idRazonS');

        $prioridades = DB::table('cat_prioridades')->get();
        $tipos_art = DB::table('cat_tipos_articulo')->get();
        $plantas_ = DB::table('Unidades_Negocio as un')
            ->join('cat_razones_sociales as rs','rs.razon_social','un.Razon_Social')
            ->select('un.id', 'un.Codigo')
            ->whereIn('rs.id',$empresasAsociadas)
        ->get();
        $unidad_medida = DB::table('cat_unidades_medida')->get();

        $plantas_preser = array();
        $plantas_seemex = array();
        $plantas = array();

        
        foreach($plantas_ as $p){
            if(substr($p->Codigo,0,4)=="PSR_"){
                $plantas_preser[]=$p;
            }else if(substr($p->Codigo,0,4)=="SMX_"){
                $plantas_seemex[]=$p;
            }else if(substr($p->Codigo,0,1)!="_"){
                $plantas[]=$p;
            }
        }
        return view('pages.template',[
            'prioridades' => $prioridades,
            'tipos_art' => $tipos_art,
            'plantas' => $plantas,
            'plantas_p'=>$plantas_preser,
            'plantas_s'=>$plantas_seemex,
            'unidades_medida' => $unidad_medida,
            'razon' => $userId
        ]);
    }

    public function edit_arts($idSol){
        $userId = Auth::user()->id;

        $isAdmin = Auth::user()->user_type;
        
        $owner = DB::table('solicitudes')->where('id', $idSol)->get('*')[0];

        if ($isAdmin!=1 and $owner->tipo=="Alta") {
            return view('home');
        }
        //dd($owner);

        $empresasAsociadas = DB::table('usuarios_empresas')->where('idUser',$owner->userId)->pluck('idRazonS');

        $prioridades = DB::table('cat_prioridades')->get();
        $tipos_art = DB::table('cat_tipos_articulo')->get();
        $plantas_ = DB::table('Unidades_Negocio as un')
            ->join('cat_razones_sociales as rs','rs.razon_social','un.Razon_Social')
            ->select('un.id', 'un.Codigo')
            ->whereIn('rs.id',$empresasAsociadas)
        ->get();
        $unidad_medida = DB::table('cat_unidades_medida')->get();
        $plantas_preser = array();
        $plantas_seemex = array();
        $plantas = array();

        foreach($plantas_ as $p){
            if(strpos($p->Codigo,"PSR_")!==false){
                $plantas_preser[]=$p;
            }else if(strpos($p->Codigo,"SMX_")!==false){
                $plantas_seemex[]=$p;
            }else{
                $plantas[]=$p;
            }
        }
        return view("pages.edit-template", [
            'prioridades' => $prioridades,
            'tipos_art' => $tipos_art,
            'plantas' => $plantas,
            'plantas_p'=>$plantas_preser,
            'plantas_s'=>$plantas_seemex,
            'unidades_medida' => $unidad_medida,
            'razon' => $owner->userId,
            'solicitud' => $idSol
        ]);
    }

    public function getArticulos($idSol){
        $res = DB::table('solicitudes as s')
        ->join('pre_articulos as p', 'p.idSolicitud', 's.id')
        ->where('s.id', $idSol)
        ->get([
            's.prioridad',
            's.tipo',
            'p.tipo as tipo_art',
            'p.aplicacion',
            'p.marca',
            'p.num_parte',
            'p.descripcion',
            'p.unidad_medida',
            'p.precio',
            'p.planta',
            'p.art_orc',
            's.comentario'
        ])->toArray();

        $temp = array();

        $des = "";
        $precio = "";
        $last_index = 0;
        foreach ($res as $index => $a) {
            if ($des == $a->descripcion && $precio == $a->precio){
                $temp[$last_index]["planta"][] = $a->planta;
            } else {
                $temp[] = array();
                $temp[$index]["planta"] = array();
                foreach ($a as $k => $v){
                    if ($k=="planta") {
                        $temp[$index][$k][] = $v;
                    } else {
                        $temp[$index][$k] = $v;
                    }
                }
                $des = $a->descripcion;
                $precio = $a->precio;
                $temp[$index]["exist"] = [$index, []];
                $temp[$index]["edit"] = $index;
                $temp[$index]["delete"] = $index;
                $temp[$index]["DT_RowId"] = $index;
                $last_index = $index;
            }
        }

        return $temp;
    }

    public function select2_marcas(Request $request){

        $term = $request->term ?: '';
        $tags = DB::table('cat_marcas')->where('marca', 'like', $term.'%')->pluck('marca', 'id');
        $valid_tags = [];
        foreach ($tags as $id => $tag) {
            $valid_tags[] = ['id' => $id, 'text' => $tag];
        }
        return \Response::json($valid_tags);
    }

    public function select2_aplicacion(Request $request){

        $term = $request->term ?: '';
        $tags = DB::table('cat_aplicaciones')->where('aplicacion', 'like', $term.'%')->pluck('aplicacion', 'id');
        $valid_tags = [];
        foreach ($tags as $id => $tag) {
            $valid_tags[] = ['id' => $id, 'text' => $tag];
        }
        return \Response::json($valid_tags);
    }
    
    public function search(Request $req){
        $desc = $req->desc;
        $desc = strtoupper($desc);
        $num_parte = $req->num_parte;
        $desc = $this->obtenerTresPalabras($desc);
        $coincidencias = [];
        $res = array();

        $coincidencias = DB::table('cat_articulos')->pluck('descripcion', 'item');

        $trie = new TrieDS();
        $trie->insert($desc);
        foreach ($coincidencias as $nm => $c) {
            if ($trie->find($c, sizeof(explode(" ", $desc))/4, 80)) {
                $res[] = [$nm, $c];
            }
        }

        return $res;
    }


    public function insert(Request $req){
        $var = $req->data;
        $userType = Auth::user()->user_type;
        $idSol = null;
        $comentario = preg_replace('/(#||-)*/','',$req->coment);
        try {
            if($var[0][1] == "Alta" && $userType == 2 && $var[0][2]!=2){
                $idSol = DB::table('solicitudes')->insertGetId([
                    'prioridad' => $var[0][0],
                    'tipo' => $var[0][1],
                    'ticket' => '',
                    'estatus' => 1,
                    'fecha_creado' => date('Y-m-d'),
                    'userId' => Auth::user()->id,
                    'comentario' => $comentario
                ]);
            }else{
                $idSol = DB::table('solicitudes')->insertGetId([
                    'prioridad' => $var[0][0],
                    'tipo' => $var[0][1],
                    'ticket' => '',
                    'estatus' => 2,
                    'fecha_creado' => date('Y-m-d'),
                    'userId' => Auth::user()->id,
                    'comentario' => $comentario
                ]);
            }
            
    
            foreach ($var as $art) {
                if (!is_numeric($art[3])) {
                    $id = DB::table('cat_aplicaciones')->where("aplicacion", $art[3])->pluck("id");
                    if (count($id)==0) {
                        $art[3] = DB::table('cat_aplicaciones')->insertGetId([
                            'aplicacion' => $art[3]
                        ]);
                    } else {
                        $art[3] = $id[0];
                    }
                }
    
                if (!is_numeric($art[4])) {
                    $id = DB::table('cat_marcas')->where("marca", $art[4])->pluck("id");
                    if (count($id)==0) {
                        $art[4] = DB::table('cat_marcas')->insertGetId([
                            'marca' => $art[4]
                        ]);
                    } else {
                        $art[4] = $id[0];
                    }
                }
                
                $temp = array(
                    'tipo' => $art[2],
                    'aplicacion' => $art[3],
                    'marca' => $art[4],
                    'num_parte' => $art[5],
                    'descripcion' => $art[6],
                    'unidad_medida' => $art[7],
                    'precio' => $art[8],
                    'planta' => '',
                    'idSolicitud' => $idSol,
                    'art_orc' => $art[10]
                );
    
                $plantas = $art[9];
                foreach ($plantas as $planta) {
                    $temp['planta'] = $planta;
                    DB::table('pre_articulos')->insert($temp);
                }
            };
        } catch (\Throwable $th) {
            if ($idSol){
                DB::table('pre_articulos')->where("idSolicitud", $idSol)->delete();
                DB::table('solicitudes')->where("id", $idSol)->delete();
            }
            return response($th);
        }

        
        return $idSol;
    }

    public function edit(Request $req){
        $var = $req->data;
        $idSol = $req->sol;
        $idUSR = Auth::user()->id;
        $userType = Auth::user()->user_type;
        $comentario = preg_replace('/(#||-)*/','',$req->coment);
        
        $owner = DB::table('solicitudes')->where('id', $idSol)->get('*')[0];
        
        
        if($userType!=1 and $owner->userId!=$idUSR){
            return response($idUSR, 401);
        }

        try {
            DB::table('pre_articulos')->where("idSolicitud", $idSol)->delete();
            
            foreach ($var as $art) {
                if (!is_numeric($art["aplicacion"])) {
                    $id = DB::table('cat_aplicaciones')->where("aplicacion", $art["aplicacion"])->pluck("id");
                    if (count($id)==0) {
                        $art["aplicacion"] = DB::table('cat_aplicaciones')->insertGetId([
                            'aplicacion' => $art["aplicacion"]
                        ]);
                    } else {
                        $art["aplicacion"] = $id[0];
                    }
                }
    
                if (!is_numeric($art["marca"])) {
                    $id = DB::table('cat_marcas')->where("marca", $art["marca"])->pluck("id");
                    if (count($id)==0) {
                        $art["marca"] = DB::table('cat_marcas')->insertGetId([
                            'marca' => $art["marca"]
                        ]);
                    } else {
                        $art["marca"] = $id[0];
                    }
                }
                
                $temp = array(
                    'tipo' => $art["tipo_art"],
                    'aplicacion' => $art["aplicacion"],
                    'marca' => $art["marca"],
                    'num_parte' => $art["num_parte"],
                    'descripcion' => $art["descripcion"],
                    'unidad_medida' => $art["unidad_medida"],
                    'precio' => $art["precio"],
                    'planta' => '',
                    'idSolicitud' => $idSol,
                    'art_orc' => $art["art_orc"]
                );
    
                $plantas = $art["planta"];
                foreach ($plantas as $planta) {
                    $temp['planta'] = $planta;
                    DB::table('pre_articulos')->insert($temp);
                }
            };
            DB::table('solicitudes')->where('id',$idSol)->update([
                'comentario'=>$comentario
                ]);
        } catch (\Throwable $th) {
            return response([], 400);
        } 

        
        return 'ok';
    }


    public function excel($id){

        return Excel::download(new TemplateExport($id), 'plantilla_articulos.xlsx');
    }
    
    //Almacenamiento para FTP
   /* public function excel(){
        $s = DB::table('solicitudes')->where('estatus',1)->value('id');

        Excel::store(new TemplateExport, '/plantillas/'.$s.'.xlsx', 'real_public');
        return redirect()->route('template');
    }*/

    public function obtenerTresPalabras($desc){
        $result = "";
        $palabras = explode(" ", $desc);

        foreach($palabras as $p){
            if(strlen($p) > 2 && preg_match("/[a-z A-Z 0-9]+/", $p)){
                $result .= " $p";
            }
        }
        return $result;
    }
    
    /// FILE START
    
    public function insertFile(Request $r){
        $message = "";
        if($r->hasFile("archivo")){
            
            $files = $r->file("archivo");
            foreach($files as $f){
                $size = ($f->getSize()/1024)/1024;
                if($size<3){
                    //dd($f->originalName);
                    $nombre = str_replace(" ", "_", $f->getClientOriginalName());
                    
                    $ruta = "archivos/".$r->id;
                    
                    Storage::delete($ruta.'/'.$nombre);
                    Storage::makeDirectory("archivos/".$r->id);

                    Storage::putFileAs($ruta, $f, $nombre);
                    $message .= "Archivo: ".$f->getClientOriginalName()." ok in $ruta";
                    DB::table("archivos_solicitud")->insert([
                        "id_solicitud" => $r->id,
                        "nombre_archivo" => $nombre
                    ]);
                } else {
                    $message .= "Tamaño del Archivo: ".$f->getClientOriginalName()." es invalido\n";
                }
            }
        } else {
            $message = "No hay archivos";
        }
        return $message;
    }
    
    public function getFiles($idSol){
        $files = DB::table("archivos_solicitud")->where('id_solicitud', '=', $idSol)->pluck('nombre_archivo');
        return $files;
    }
    
    public function downloadFile($id, $file){
        return Storage::download("archivos/$id/$file");
    }
}
