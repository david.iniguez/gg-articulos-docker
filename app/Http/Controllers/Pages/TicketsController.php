<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\DB as FacadesDB;

class TicketsController extends Controller
{
    public function form(){
        $userId =  Auth::user()->id;
        $typeUser = Auth::user()->user_type;
        $status = DB::table('estatus')->where('Descripcion_2','<>',"")->get();

        if($typeUser == 1){
            $raz = DB::table('usuarios_empresas')->where('idUser', $userId)->get('idRazonS');
            $usersRelacionados = [];
            foreach($raz as $r){
                $idU = DB::table('usuarios_empresas')
                ->join('users','users.id','usuarios_empresas.idUser')
                ->where([
                    ['idRazonS',$r->idRazonS],
                    ['users.user_type',2]
                ])->value('users.id');
                
                if(!is_null($idU)){
                    array_push($usersRelacionados,$idU);
                }
            }
            array_push($usersRelacionados,$userId);
            $sols = DB::table('solicitudes')
            ->join('cat_prioridades','solicitudes.prioridad','cat_prioridades.id')
            ->join('estatus','estatus.id','solicitudes.estatus')
            ->join('users','solicitudes.userId','users.id')
            ->select(
                'solicitudes.id',
                'solicitudes.ticket',
                'cat_prioridades.prioridad',
                'solicitudes.tipo',
                'solicitudes.fecha_creado',
                'estatus.Descripcion_2 as status',
                'users.name'
            )
            ->whereIn('userId',$usersRelacionados)
            ->get();
        }else{
            $sols = DB::table('solicitudes')
            ->join('cat_prioridades','solicitudes.prioridad','cat_prioridades.id')
            ->join('estatus','estatus.id','solicitudes.estatus')
            ->join('users','solicitudes.userId','users.id')
            ->select(
                'solicitudes.id',
                'solicitudes.ticket',
                'cat_prioridades.prioridad',
                'solicitudes.tipo',
                'solicitudes.fecha_creado',
                'estatus.Descripcion_2 as status',
                'users.name'
            )
            ->where('userId',$userId)
            ->get();
        }
        

        return view('pages.tickets',[
            'sols' => $sols,
            'estatus' => $status,
            'stat' => '0',
            'typeUser' => $typeUser
        ]);
    }

    public function search(Request $r){
        $userId =  Auth::user()->id;
        $typeUser = Auth::user()->user_type;
        $fechaI = $r->fecha_ini;
        $fechaF = $r->fecha_fin;
        $stat = $r->status;

        if($stat != 99){
            $status = DB::table('estatus')->where('Id','<>',$stat)->get();

            if($typeUser == 1){
                $raz = DB::table('usuarios_empresas')->where('idUser', $userId)->get('idRazonS');
                $usersRelacionados = [];
                foreach($raz as $r){
                    $idU = DB::table('usuarios_empresas')
                    ->join('users','users.id','usuarios_empresas.idUser')
                    ->where([
                        ['idRazonS',$r->idRazonS],
                        ['users.user_type',2]
                    ])->value('users.id');
                    
                    if(!is_null($idU)){
                        array_push($usersRelacionados,$idU);
                    }
                }
                array_push($usersRelacionados,$userId);
                $sols = DB::table('solicitudes')
                ->join('cat_prioridades','solicitudes.prioridad','cat_prioridades.id')
                ->join('estatus','estatus.id','solicitudes.estatus')
                ->join('users','solicitudes.userId','users.id')
                ->select(
                    'solicitudes.id',
                    'solicitudes.ticket',
                    'cat_prioridades.prioridad',
                    'solicitudes.tipo',
                    'solicitudes.fecha_creado',
                    'estatus.Descripcion_2 as status',
                    'users.name'
                )
                ->whereIn('userId',$usersRelacionados)
                ->where('estatus', $stat)
                ->whereBetween('fecha_creado',[$fechaI, $fechaF])
                ->get();
            }else{
                $sols = DB::table('solicitudes')
                ->join('cat_prioridades','solicitudes.prioridad','cat_prioridades.id')
                ->join('estatus','estatus.id','solicitudes.estatus')
                ->join('users','solicitudes.userId','users.id')
                ->select(
                    'solicitudes.id',
                    'solicitudes.ticket',
                    'cat_prioridades.prioridad',
                    'solicitudes.tipo',
                    'solicitudes.fecha_creado',
                    'estatus.Descripcion_2 as status',
                    'users.name'
                )
                ->where([
                    ['userId', $userId],
                    ['estatus', $stat]
                ])
                ->whereBetween('fecha_creado',[$fechaI, $fechaF])
                ->get();
            }

            $statV = DB::table('estatus')->where('Id',$stat)->get();
                
            return view('pages.tickets',[
                'sols' => $sols,
                'estatus' => $status,
                'stat' => $statV[0],
                'typeUser' => $typeUser
            ]);
        }else{
            $status = DB::table('estatus')->get();

            if($typeUser == 1){
                $raz = DB::table('usuarios_empresas')->where('idUser', $userId)->get('idRazonS');
                $usersRelacionados = [];
                foreach($raz as $r){
                    $idU = DB::table('usuarios_empresas')
                    ->join('users','users.id','usuarios_empresas.idUser')
                    ->where([
                        ['idRazonS',$r->idRazonS],
                        ['users.user_type',2]
                    ])->value('users.id');
                    
                    if(!is_null($idU)){
                        array_push($usersRelacionados,$idU);
                    }
                }
                array_push($usersRelacionados,$userId);
                $sols = DB::table('solicitudes')
                ->join('cat_prioridades','solicitudes.prioridad','cat_prioridades.id')
                ->join('estatus','estatus.id','solicitudes.estatus')
                ->join('users','solicitudes.userId','users.id')
                ->select(
                    'solicitudes.id',
                    'solicitudes.ticket',
                    'cat_prioridades.prioridad',
                    'solicitudes.tipo',
                    'solicitudes.fecha_creado',
                    'estatus.Descripcion_2 as status',
                    'users.name'
                )
                ->whereIn('userId',$usersRelacionados)
                ->whereBetween('fecha_creado',[$fechaI, $fechaF])
                ->get();
            }else{
                $sols = DB::table('solicitudes')
                ->join('cat_prioridades','solicitudes.prioridad','cat_prioridades.id')
                ->join('estatus','estatus.id','solicitudes.estatus')
                ->join('users','solicitudes.userId','users.id')
                ->select(
                    'solicitudes.id',
                    'solicitudes.ticket',
                    'cat_prioridades.prioridad',
                    'solicitudes.tipo',
                    'solicitudes.fecha_creado',
                    'estatus.Descripcion_2 as status',
                    'users.name'
                )
                ->where([
                    ['userId', $userId]
                ])
                ->whereBetween('fecha_creado',[$fechaI, $fechaF])
                ->get();
            }
                
            return view('pages.tickets',[
                'sols' => $sols,
                'estatus' => $status,
                'stat' => '0',
                'typeUser' => $typeUser
            ]);
        }
    }

    public function detail(Request $r){
        $idSol = $r->idSol;

        $articulos = DB::table('solicitud_articulos2')->where('id',$idSol)->get();

        return json_encode($articulos);
    }

    public function aprobar(Request $request){

        foreach ($request->selected as $s){
            //echo $sale." ";

            DB::table('solicitudes')
            ->where('id',$s)
            ->update(['estatus' => 2, 'user_valida' => Auth::user()->id]);
        }

        return $this->form();

    }

}
