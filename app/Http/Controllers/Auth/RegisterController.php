<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'password_not_hash' => $data['password'],
        ]);
    }

    public function form(){

        $empresas = DB::table('cat_razones_sociales')->get();
        return view('auth.register',[
            'rs' => $empresas
        ]);
    }

    public function insert(Request $r){
        DB::table('users')
        ->insert([
            'name' => $r->name,
            'email' => $r->email,
            'password' => Hash::make($r->password),
            'password_not_hash' => $r->password,
            'user_type' => $r->userType
        ]);

        $idU = DB::table('users')->max('id');
        
        if(!is_null($r->rs1)){
            DB::table('usuarios_empresas')
            ->insert([
                'idUser' => $idU,
                'idRazonS' => $r->rs1
            ]);
        }
        
        if(!is_null($r->rs2)){
            DB::table('usuarios_empresas')
            ->insert([
                'idUser' => $idU,
                'idRazonS' => $r->rs2
            ]);
        }
        
        if(!is_null($r->rs3)){
            DB::table('usuarios_empresas')
            ->insert([
                'idUser' => $idU,
                'idRazonS' => $r->rs3
            ]);
        }

        if(!is_null($r->rs4)){
            DB::table('usuarios_empresas')
            ->insert([
                'idUser' => $idU,
                'idRazonS' => $r->rs4
            ]);
        }

        if(!is_null($r->rs5)){
            DB::table('usuarios_empresas')
            ->insert([
                'idUser' => $idU,
                'idRazonS' => $r->rs5
            ]);
        }

        if(!is_null($r->rs6)){
            DB::table('usuarios_empresas')
            ->insert([
                'idUser' => $idU,
                'idRazonS' => $r->rs6
            ]);
        }

        if(!is_null($r->rs7)){
            DB::table('usuarios_empresas')
            ->insert([
                'idUser' => $idU,
                'idRazonS' => $r->rs7
            ]);
        }

        if(!is_null($r->rs8)){
            DB::table('usuarios_empresas')
            ->insert([
                'idUser' => $idU,
                'idRazonS' => $r->rs8
            ]);
        }

        if(!is_null($r->rs9)){
            DB::table('usuarios_empresas')
            ->insert([
                'idUser' => $idU,
                'idRazonS' => $r->rs9
            ]);
        }

        if(!is_null($r->rs10)){
            DB::table('usuarios_empresas')
            ->insert([
                'idUser' => $idU,
                'idRazonS' => $r->rs10
            ]);
        }

        if(!is_null($r->rs11)){
            DB::table('usuarios_empresas')
            ->insert([
                'idUser' => $idU,
                'idRazonS' => $r->rs11
            ]);
        }

        if(!is_null($r->rs12)){
            DB::table('usuarios_empresas')
            ->insert([
                'idUser' => $idU,
                'idRazonS' => $r->rs12
            ]);
        }

        return redirect(Route('login'));
    }
}
