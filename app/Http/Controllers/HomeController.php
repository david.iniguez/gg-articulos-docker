<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB as FacadesDB;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $tipo = Auth::user()->user_type; 
        $userId =  Auth::user()->id;
        $empresasAsociadas = DB::table('usuarios_empresas')->join('users','users.id','usuarios_empresas.idUser')
            ->where([
                ['idRazonS',$userId]
        ])->pluck('users.id');
        $status = DB::table('solicitudes')
            ->join('users','solicitudes.userId','users.id')
            ->whereIn('userId',$empresasAsociadas)
        ->where(['estatus'=> 1, "userId" => $userId])->count();
       
        return view('home',["estatus_sol"=> $status,'type_user'=>$tipo
        ]);
    }
}
