<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'insertTicket',
        'insertArticulo',
        'insertArticuloCat',
        'insertMarcaCat',
        'articulos/get',
        'articulos/insert',
        'articulos/edit',
        "file/insert",
        'updateEstatusMarca'
    ];
}
